const user = require("../models/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new user({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		}
		else {
			return true;
		};
	});
};

// Check Email
module.exports.checkEmailExists = (reqBody) => {
	return user.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};

// User authentication
module.exports.loginUser = (reqBody) => {
	return user.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		} 
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				return { access : auth.createAccessToken(result) }
			}
			else {
				return false;
			};
		};
	});
};

// Retrieve user details
module.exports.getProfile = (data) => {
	return user.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

// Set user as Admin
module.exports.setAsAdmin = (reqParams, userData) => {
	if(userData.isAdmin){
		return user.findById(reqParams.userId.toLowerCase()).then(result => {
			// if user id is already an admin
			if(result.isAdmin == true){
				return false;
			}
			else{
				let updatedUser = {
					isAdmin : true
				}
				// update isAdmin value
				return user.findByIdAndUpdate(reqParams.userId.toLowerCase(), updatedUser).then((user, err) => {
					if(err){
						return false;
					}
					else{
						return true;
					}
				});
			}
		})
	}
	else{
		return Promise.resolve(false);
	}
}