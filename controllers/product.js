const product = require("../models/product");

// Register a product
module.exports.createProduct = (req, userData) => {
	if(userData.isAdmin){
		let newProduct = new product({
			name : req.body.name.toLowerCase(),
			description : req.body.description,
			price : req.body.price,
			quantity : req.body.quantity,
			image : req.body.image
		});
		return newProduct.save().then((product, err) => {
			if(err){
				return false;
			}
			else{
				return true;
			}
		});
	}
	else{
		return Promise.resolve(false);
	}
}

// Get all products (admin only)
module.exports.getAllProducts = (userData) => {
	if(userData.isAdmin){
		return product.find({}).then(result =>{
			// if no registered product
			if(result.length == 0){
				return false;
			}
			else{
				return result;
			}
		})
	}
	else{
		return Promise.resolve(false);
	}
}

// Get all Active Products
module.exports.getAllActiveProducts = () => {
	return product.find({isActive : true}).then(result =>{
		// if no registered product
		if(result.length == 0){
			return false;
		}
		else{
			return result;
		}
	})
}

// Get a specific Product
module.exports.getProduct = (reqParams) => {
	return product.findById(reqParams.productId).then(result =>{
		// if id is not found
		if(!result){
			return false;
		}
		else{
			return result;
		}
	})
}

// Updating a product
module.exports.updateProduct = (reqParams, data) => {
	if(data.isAdmin){
		let updatedProduct = {
			name : data.product.name,
			description : data.product.description,
			price : data.product.price,
			quantity : data.product.quantity,
			image : data.product.image,
			isActive : data.product.isActive
		}
		return product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return Promise.resolve(false);
	}
}

// Archive a product
module.exports.archiveProduct = (reqParams, data) => {
	if(data.isAdmin){
		let updatedProduct = {
			isActive : false
		}
		return product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return Promise.resolve(false);
	}
}
