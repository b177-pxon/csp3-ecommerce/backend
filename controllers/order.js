const order = require("../models/order");
const product = require("../models/product");
const user = require("../models/user");

// Checkout order
module.exports.order = (data) => {
	if(!data.isAdmin){
		// check if product id exists
		return product.find({_id : data.productId}).then(result => {
			// if no registered product id
			if(result.length == 0){
				return false;
			}
			else{
				// compute total price
				let total = data.quantity * result[0].price;
				const newOrder = new order({
					userId : data.userId,
					userFirstName : data.firstName,
					userLastName : data.lastName,
					productId : data.productId,
					productName : result[0].name,
					quantity : data.quantity,
					image : result[0].image,
					totalAmount : total
				});

				// if product quantity < input quantity
				if((result[0].quantity < data.quantity) || data.quantity == 0){
					return false;
				}
				else{
					return newOrder.save().then((order, err) => {
					if(err){
						return err;
					}
					else{
						let isProductQtyUpdated = product.findById(data.productId).then(productQty => {
							let newQuantity =  productQty.quantity - data.quantity;
							if(newQuantity == 0){
								const newProduct = {
									quantity : newQuantity,
									isActive : false
								};
								return product.findByIdAndUpdate(data.productId, newProduct).then((Product, err) => {
									if(err){
										return err;
									}
									else{
										return true;
									}
								})
							}
							else {
								const newProduct = {
									quantity : newQuantity
								};

								return product.findByIdAndUpdate(data.productId, newProduct).then((Product, err) => {
									if(err){
										return err;
									}
									else{
										return true;
									}
								})
							}
						})
						if(isProductQtyUpdated){
							return true;
						}
						else{
							return false;
						}
					}
					});
				}}
		})
	}
		else{
		return Promise.resolve(false);
	}
}


// Get all orders (Admin only)
module.exports.getAllOrders = (userData) => {
	if(userData.isAdmin){
		return order.find({}).then(result =>{
			// if no registered order
			if(result.length == 0){
				return false;
			}
			else{
				return result;
			}
		})
	}
	else{
		return Promise.resolve(false);
	}
}

// Get all orders for user
module.exports.getOrders = (userData) => {
	return order.find({userId : userData.userId}).then(result =>{
		// if no registered order
		if(result.length == 0){
			return false;
		}
		else{
			return result;
		}
	})
}