const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
    productId : {
        type : String,
        required : [true, "Product ID Required."]
    },
    productName : {
        type : String
    },
    userId : {
        type : String
    },
    userFirstName : {
        type : String
    },
    userLastName : {
        type : String
    },
    quantity : {
        type : Number,
        required : [true, "Quantity Required."]
    },
    totalAmount : {
        type : Number
    },
    purchasedOn : {
        type : Date,
        default : Date.now
    },
    image : {
        type : String
    }
});

module.exports = mongoose.model("order", orderSchema);