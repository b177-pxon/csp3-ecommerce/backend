const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name : {
        type : String,
        required : [true, "Product name is required."]
    },
    description : {
        type : String,
        required : [true, "Product description is Required."]
    },
    price : {
        type : Number,
        required : [true, "Price is Required."]
    },
    quantity : {
        type : Number,
        required : [true, "Quantity is Required."]
    },
    isActive : {
        type : Boolean,
        default : true
    },
    createdOn : {
        type : Date,
        default : Date.now
    },
    image : {
        type : String
    }
});

module.exports = mongoose.model("product", productSchema);