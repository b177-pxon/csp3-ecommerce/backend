const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Creating a product
router.post("/add", auth.verify, (req, res) => {
    const userData = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
    productController.createProduct(req, userData).then(resultFromController => res.send(resultFromController));
});

// Get all products (admin only)
router.get("/all", auth.verify, (req, res) => {
    const userData = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
    productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
})

// Get all Active Products
router.get("/", (req, res) => {
    productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController))
})

// Get a specific product
router.get("/:productId", (req, res) => {
    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})

// Updating a product (admin only)
router.put("/:productId", auth.verify, (req, res) => {
    const data = {
        product : req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
    productController.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController))
})

// Archive Product (admin only)
router.put("/:productId/archive", auth.verify, (req, res) => {
    const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
    productController.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController))
})

module.exports = router;