const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require("../auth");

// Check out order
router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id,
		firstName : auth.decode(req.headers.authorization).firstName,
		lastName : auth.decode(req.headers.authorization).lastName,
		productId : req.body.productId,
		quantity : req.body.quantity
	}
	orderController.order(data).then(resultFromController => res.send(resultFromController));
})

// Get all order (admin only)
router.get("/orders", auth.verify, (req, res) => {
    const userData = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
    orderController.getAllOrders(userData).then(resultFromController => res.send(resultFromController))
})

// Get user orders
router.get("/myOrders", auth.verify, (req, res) => {
    const userData = {
        userId : auth.decode(req.headers.authorization).id
	}
    orderController.getOrders(userData).then(resultFromController => res.send(resultFromController))
})

module.exports = router;