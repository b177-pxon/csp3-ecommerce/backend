const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

// Register an account
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Check Email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details

router.get("/details", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	userController.getProfile({userId : data.id}).then(resultFromController => res.send(resultFromController));
});

// Set user as Admin
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
    const userData = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
	}
    userController.setAsAdmin(req.params, userData).then(resultFromController => res.send(resultFromController));
});

module.exports = router;